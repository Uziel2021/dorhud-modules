local module = DorHUDMod:new("Offline_Chat", { abbr = "OC",
	author = "Shield Hater", version = "Dorhud Edition", description = {
		english = "Enable Use Chat In Single Player",
        spanish = "Permite Usar El Chat En Un Jugador"
    }
})

module:register_post_override("lib/managers/menumanager", "menumanager")

return  module