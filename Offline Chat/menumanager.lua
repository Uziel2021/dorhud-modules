function MenuManager:toggle_chatinput()
	if SystemInfo:platform() ~= Idstring("WIN32") then
		return
	end
	if self:active_menu() then
		return
	end
	if managers.hud then
		managers.hud:toggle_chatinput()
	end
end