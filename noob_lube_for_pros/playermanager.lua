function PlayerManager:availible_crew_bonuses(slot) --Noob lube will be always available
	if not self._global.upgrades.crew_bonus then
		return {}
	end
	local bonuses = {}
	for name,_ in pairs(self._global.upgrades.crew_bonus) do
		table.insert(bonuses, name)
	end
	return bonuses
end