local module = DorHUDMod:new("Drill_In_Minutes", { abbr = "DIM",
	author = "Shield Hater", version = "dorhud edition", description = {
		english = "Show The Drill Time In Minutes | H:M:S",
        spanish = "Muestra El Tiempo De Taladro En Minutos | H:M:S"
    }
})

module:register_post_override("lib/units/props/timergui", "timergui")

return module