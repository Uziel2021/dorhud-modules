function TimerGui:update(unit, t, dt)
	if self._jammed then
		self._gui_script.drill_screen_background:set_color(self._gui_script.drill_screen_background:color():with_alpha(0.5 + (math.sin(t * 750) + 1) / 4))
		return
	end

	if not self._powered then
		return
	end

	if self._current_jam_timer then
		self._current_jam_timer = self._current_jam_timer - dt
		if self._current_jam_timer <= 0 then
			self:set_jammed(true)
			self._current_jam_timer = table.remove(self._jamming_intervals, 1)
			return
		end
	end

	self._current_timer = self._current_timer - dt
	self._gui_script.time_text:set_text(math.floor(self._current_timer) .. " " .. managers.localization:text("prop_timer_gui_seconds"))
	self._gui_script.timer:set_w(self._timer_lenght * (1 - self._current_timer / self._timer))
	if 0 >= self._current_timer then
		self._unit:set_extension_update_enabled(Idstring("timer_gui"), false)
		self._update_enabled = false
		self:done()
	else
		self._gui_script.working_text:set_color(self._gui_script.working_text:color():with_alpha(0.5 + (math.sin(t * 750) + 1) / 4))
	end

	local basesec = math.floor(self._current_timer)
	local minutes = math.round(math.floor(basesec/60))
	local seconds = basesec - (minutes * 60)	
	if seconds < 10 then
		self._gui_script.time_text:set_text(minutes..":0"..seconds.." ["..basesec.."]")
	else
		self._gui_script.time_text:set_text(minutes..":"..seconds.." ["..basesec.."]")
	end
end