local module = DorHUDMod:new("One_Weapon_Slot", { abbr = "OWS",
	author = "gir489", version = "dorhud edition", description = {
		english = "Set all weapons into a single slot.",
    }
})

module:register_post_override("lib/managers/playermanager", function()
	PlayerManager.WEAPON_SLOTS = 1
end)

module:register_post_override("lib/tweak_data/weapontweakdata", function()
	local orig = WeaponTweakData.init
	function WeaponTweakData:init()
		orig(self)
		self.hk21.use_data.selection_index = 1
		self.r870_shotgun.use_data.selection_index = 1
		self.m14.use_data.selection_index = 1
		self.m4.use_data.selection_index = 1
		self.ak47.use_data.selection_index = 1
		self.mossberg.use_data.selection_index = 1
		self.mp5.use_data.selection_index = 1
		self.mac11.use_data.selection_index = 1
		self.m79.use_data.selection_index = 1
	end
end)

return module