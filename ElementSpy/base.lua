local module = DMod:new("ElementSpy", {
	name = "Element Spy",
	version = "1",
	author = "_atom",
})

module:hook_post_require("lib/managers/mission/missionscriptelement", "ElementSpy", 668, false)
module:hook_post_require("lib/managers/missionmanager", "ElementSpy", 668, false)

return module