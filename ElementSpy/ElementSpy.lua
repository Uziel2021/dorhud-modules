--------------------
-- ElementSpy.lua --
--------------------
--[[
	This file contains the mission script (elements) analizer.

	Notes:
		- This script will inspect all the executed mission scripts in real time in order to
		[...] show a message when the listed ones are executed
		- Some of the events can only be filtered if you are the host of the session.
--]]

local elements = {
	["bank"] = {
		[104136] = { text = "The overvault has spawned, you can place the drill now." }
	},
	["suburbia"] = {
		[102096] = { text = "There is money at the basement.",    sync = true },
		[101608] = { text = "There is no money at the basement.", sync = true }
	}
}


function message(text, sync)
	local local_peer = managers.network:session():local_peer()
	local say = string.format("%s: %s", string.upper(local_peer:name()), text)

	managers.menu:relay_chat_message(say, local_peer:id())
	if sync then
		managers.network:session():send_to_peers("sync_chat_message", say)
	end
end

if RequiredScript == "lib/managers/mission/missionscriptelement" then
	function MissionScriptElement:spy()
		local id = self._id
		local level = Global.level_data.level_id

		if elements[level] and elements[level][id] then
			local element = elements[level][id]
			message(element.text, (element.sync or false))
		end
	end


	local orig_exec = MissionScriptElement.on_executed
	function MissionScriptElement:on_executed(...)
		orig_exec(self, ...)
		if Network:is_server() then
			self:spy()
		end
	end
end

if RequiredScript == "lib/managers/missionmanager" then
	local orig_clt = MissionManager.client_run_mission_element
	function MissionManager:client_run_mission_element(id, unit)
		orig_clt(self, id, unit)
		if Network:is_client() then
			local element = self:get_element_by_id(id)
			if element then
				element:spy()
			end
		end
	end
end