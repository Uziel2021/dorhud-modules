local orig = HUDManager.update
function HUDManager:update(t, dt)
	orig(self, t, dt)
	local info_hud = self:script(PlayerBase.PLAYER_INFO_HUD)
	local player = managers.player:player_unit()
	if not info_hud or not player then return end
	self:_Change_String(info_hud, player)
end

function HUDManager:_Change_String(hud, player)
	local current = managers.criminals:local_character_name()
	local text = ""

	if current == "russian" then -- use uppercases or add "string":upper() if you want
		text = "Dallas"
	elseif current == "american" then
		text = "Hoxton"
	elseif current == "german" then
		text = "Wolf"
	elseif current == "spanish" then
		text = "Chains"
	end
		
	hud.health_name:set_text(text)
end