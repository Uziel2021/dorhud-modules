local orig = Setup.init_finalize

function Setup:init_finalize(...)
	orig(self, ...)
	self._framerate_cap = 30 -- Change this
	Application:cap_framerate(self._framerate_cap)
end
